var process = require('child_process');
var execFile = process.execFile;
var fs = require('fs');
var page = require('webpage').create();

var outdir = './outdir';

if(fs.isDirectory(outdir)) {
    fs.removeTree(outdir);
}

if(!fs.makeDirectory(outdir)) {
    throw 'Error creating directory: ' + outdir;
}

var baseUrl = 'http://newt.phys.unsw.edu.au/music/saxophone/tenor';
var graphicsUrl = [
    baseUrl, 'graphics'
].join('/');

var notes = [
    'C', 'D', 'E', 'F', 'G', 'A', 'B'
];

var ignoreSharpNotes = [
    'E', 'B'
];

function Note(options) {
    options = options || {};

    if(!options.note) {
        throw 'Note must have a note value';
    }

    if(!options.transposition) {
        throw 'Note must have a transposition value';
    }

    this.note = options.note;
    this.transposition = options.transposition;
    this.sharp = Boolean(options.sharp);
    this.fileNamePrefix = null;

    this._urlFileName = null;
    this._fileName = null;
    this._fileDir = null;

    this._mp3FilePath = null;
    this._impFilePath = null;
    this._soundFilePath = null;

    this._mp3Url = null;
    this._impUrl = null;
    this._soundUrl = null;
}

Object.defineProperty(Note.prototype, 'urlFileName', {
    get: function() {
        if(!this._urlFileName) {
            var fileName = this.note;

            if(this.sharp) {
                fileName += 'sharp';
            }

            fileName += this.transposition;

            this._urlFileName = fileName;
        }

        return this._urlFileName;
    }
});

Object.defineProperty(Note.prototype, 'fileName', {
    get: function() {
        if(!this._fileName) {
            var fileName = this.urlFileName;

            if(this.fileNamePrefix) {
                fileName = this.fileNamePrefix + fileName;
            }

            this._fileName = fileName;
        }

        return this._fileName;
    }
});

Object.defineProperty(Note.prototype, 'fileDir', {
    get: function() {
        if(!this._fileDir) {
            this._fileDir = './outdir/' + this.fileName;
        }

        return this._fileDir;
    }
});

Object.defineProperty(Note.prototype, 'mp3FilePath', {
    get: function() {
        if(!this._mp3FilePath) {
            this._mp3FilePath = [
                this.fileDir, this.fileName + '.mp3'
            ].join('/');
        }

        return this._mp3FilePath;
    }
});

Object.defineProperty(Note.prototype, 'mp3Url', {
    get: function() {
        if(!this._mp3Url) {
            this._mp3Url = [
                baseUrl, 'sounds', this.urlFileName + '.mp3'
            ].join('/');
        }

        return this._mp3Url;
    }
});

Object.defineProperty(Note.prototype, 'impFilePath', {
    get: function() {
        if(!this._impFilePath) {
            this._impFilePath = [
                this.fileDir, this.fileName + '-imp-graph.gif'
            ].join('/');
        }

        return this._impFilePath;
    }
});

Object.defineProperty(Note.prototype, 'impUrl', {
    get: function() {
        if(!this._impUrl) {
            this._impUrl = [
                graphicsUrl, this.urlFileName + '.imp.gif'
            ].join('/');
        }

        return this._impUrl;
    }
});

Object.defineProperty(Note.prototype, 'soundFilePath', {
    get: function() {
        if(!this._soundFilePath) {
            this._soundFilePath = [
                this.fileDir, this.fileName + '-sound-graph.gif'
            ].join('/');
        }

        return this._soundFilePath;
    }
});

Object.defineProperty(Note.prototype, 'soundUrl', {
    get: function() {
        if(!this._soundUrl) {
            this._soundUrl = [
                graphicsUrl, this.urlFileName + '.sound.gif'
            ].join('/');
        }

        return this._soundUrl;
    }
});

var startNote = new Note({
    note: 'A',
    transposition: 3,
    sharp: true
});

var endNote = new Note({
    note: 'F',
    transposition: 7,
    sharp: true
});

var hasStarted = false;
var startNoteIndex = notes.indexOf(startNote.note);

var parseNotes;
var currentNoteIndex = -1;

run();

function run() {
    parsedNotes = buildNotes();

    downloadNext(function() {
        console.log('Finished downloading webpages');

        phantom.exit();
    });
}

function downloadNext(cb) {
    if(currentNoteIndex === parsedNotes.length - 1) {
        cb();

        return;
    }

    ++currentNoteIndex;

    var note = parsedNotes[currentNoteIndex];
    note.fileNamePrefix = currentNoteIndex + '_';

    fs.makeDirectory(note.fileDir);

    _downloadImpGraph(note, function(){
        _downloadSoundGraph(note, function() {
            _downloadMp3(note, function() {
                downloadNext(cb);
            });
        });
    });
}

function _downloadImpGraph(note, cb) {
    downloadFile(note.impFilePath, note.impUrl, cb);
}

function _downloadSoundGraph(note, cb) {
    downloadFile(note.soundFilePath, note.soundUrl, cb);
}

function _downloadMp3(note, cb) {
    downloadFile(note.mp3FilePath, note.mp3Url, cb);
}

function downloadFile(path, url, cb) {
    console.log('Downloading ' + url + ' to: ' + path);

    execFile('wget', ['-O', path, url], null, function(err, stdOut, stdErr) {
        if(err){
            console.log('wget err: ' + err);
        }

        cb();
    });
}

function buildNotes() {
    var parsedNotes = [];

    for(var trans = startNote.transposition; trans <= endNote.transposition; ++trans) {
        for(var i = 0; i < notes.length; ++i) {
            var noteLetter = notes[i];

            var isFirstTrans = trans === startNote.transposition;
            var isFirstNote = noteLetter === startNote.note;

            var isLastTrans = trans === endNote.transposition;
            var isLastNote = noteLetter === endNote.note;

            if(!hasStarted && isFirstTrans) {
                if(isFirstNote) {
                    hasStarted = true;
                } else {
                    continue;
                }
            }

            var note;
            var skipSharp = false;

            if(isFirstNote && isFirstTrans) {
                note = startNote;

                if(note.sharp) {
                    skipSharp = true;
                }
            } else {
                note = new Note({
                    note: noteLetter,
                    transposition: trans
                });
            }

            parsedNotes.push(note);

            if(ignoreSharpNotes.indexOf(noteLetter) !== -1) {
                skipSharp = true;
            }

            if(isLastNote && isLastTrans && !endNote.sharp) {
                break;
            }

            if(skipSharp) {
                continue;
            }

            note = new Note({
                note: note.note,
                transposition: note.transposition,
                sharp: true
            });

            parsedNotes.push(note);

            if(isLastNote && isLastTrans) {
                break;
            }
        }
    }

    return parsedNotes;
}
